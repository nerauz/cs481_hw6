﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace cs481_hw6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        HttpClient _client;
        string _base_uri = "https://owlbot.info/api/v4/dictionary/";
        Definition definition;
        string _token = "4a703480e1683b24892965a956b217dfdfe2f6e1";
        
        // Instantiate the HTTP client with the header asked from the owlbot api
        public MainPage()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", _token);
            InitializeComponent();
        }

        // Fonctiion which check if internet is available
        private bool is_internet_available()
        {
            var networkAccess = Connectivity.NetworkAccess;

            return networkAccess == NetworkAccess.Internet;
        }

        // Handler of the on clicked button event which communicate with the owlbot api
        public async void search(object sender, EventArgs args)
        {
            var uri = new Uri(string.Format(this._base_uri + this.input.Text, string.Empty));

            if (is_internet_available())
            {
                // Sending the get request
                var response = await _client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    // Parsing the request content
                    var content = await response.Content.ReadAsStringAsync();

                    try
                    {
                        // Deserialized the parsed content with the Definition model
                        definition = JsonConvert.DeserializeObject<Definition>(content);
                        display();
                    }
                    catch
                    {
                        await DisplayAlert("Error", "Eorre while serializatings", "OK");
                    }
                } else
                {
                    await DisplayAlert("Error", response.ReasonPhrase, "OK");
                }
            } else
            {
                await DisplayAlert("Error", "Please connect to internet", "OK");
            }
        }

        // Function which create a label for the ViewCell
        public Label create_label(string name)
        {
            Label label = new Label();

            label.SetBinding(Label.TextProperty, name);
            label.VerticalOptions = LayoutOptions.FillAndExpand;
            label.HorizontalOptions = LayoutOptions.FillAndExpand;
            return label;
        }

        // Function which create the cell for the ListView
        public ViewCell create_cell()
        {
            return new ViewCell
            {
                View = new StackLayout
                {
                    Padding = 5,
                    Children =
                    {
                        create_label("type"),
                        create_label("definition"),
                        create_label("example")
                    }
                }
            };
        }

        // Function which display the filled listview and delete the last one if needed
        public void display()
        {
            // Created the ListView
            ListView listView = new ListView
            {
                ItemsSource = definition.definitions,
                ItemTemplate = new DataTemplate(this.create_cell),
                HasUnevenRows = true,
                SeparatorColor = Color.Black
            };

            // Check if the listview is already filled
            if (this.main_stack.Children.Count >= 3)
            {
                this.main_stack.Children.RemoveAt(this.main_stack.Children.Count - 1);
                this.main_stack.Children.RemoveAt(this.main_stack.Children.Count - 1);
            }

            // Add the new elems to the main stack
            this.main_stack.Children.Add(new Label { Text = this.input.Text, FontSize = 28, FontAttributes = FontAttributes.Bold });
            this.main_stack.Children.Add(listView);
        }
    }
}
