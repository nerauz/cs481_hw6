﻿using System.Collections.Generic;

namespace cs481_hw6
{
    internal class Definition
    {
        public IList<Word> definitions { get; set; }
    }
}