﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace cs481_hw6
{
    class Word
    {
        public string type { get; set; } 
        public string definition { get; set; } 
        public string example { get; set; } 
        public string image_url { get; set; } 
        public string emoji { get; set; }

        [OnError]
        internal async void OnError(StreamingContext context, ErrorContext errorContext)
        {
            System.Console.WriteLine("Serialization error");
            errorContext.Handled = true;
        }
    }
}
